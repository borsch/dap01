package com.mathpar.NAUKMA.kurpiak;

import mpi.MPI;
import mpi.MPIException;

public class Lab2 {

    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int myrank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();

        if (myrank == 0) {
            int initialNumber = Integer.parseInt(args[0]);
            int[] result = new int[] { initialNumber };

            if (size > 1) {
                MPI.COMM_WORLD.send(result, 1, MPI.INT, myrank + 1, 1);
                MPI.COMM_WORLD.recv(result, 1, MPI.INT, size - 1, 1);
            }

            System.out.println("Final result: " + result[0]);
        } else {
            int[] buffer = new int[1];
            MPI.COMM_WORLD.recv(buffer, 1, MPI.INT, myrank - 1, 1);

            if (myrank % 2 == 0) {
                buffer[0] += myrank;
            } else {
                buffer[0] += 10 * myrank;
            }

            int target = (myrank + 1) % size;
            MPI.COMM_WORLD.send(buffer, 1, MPI.INT, target, 1);
        }

        MPI.Finalize();
    }

}
